/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>
#include <modal_pipe_client.h>

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration that's specific to voxl-qvio-server.\n\
 *\n\
 * voxl-qvio-server also uses parameters from the following shared config files:\n\
 * /etc/modalai/extrinsics.conf\n\
 * /etc/modalai/cameras.cal\n\
 *\n\
 * parameter descriptions:\n\
 * \n\
 * imu_name: VOXL uses imu1 by defualt since it's the most reliable. On\n\
 * VOXL-FLIGHT you can optionally try imu0 which is a newer icm42688\n\
 *\n\
 * cam_name: camera to use, defaults to tracking0\n\
 *\n\
 * odr_hz: Output data date is independent from the camera frame rate so you can\n\
 * choose the desired output data rate. Note that voxl-imu-server defaults to\n\
 * 500hz imu sampling but new data is received by voxl-qvio-server at 100hz by\n\
 * default so requesting qvio data faster requires updating voxl-imu-server.\n\
 *\n\
 * use_camera_height_bootstrap: When enabled, the feature estimator will assume\n\
 * the system starts up with the camera pointed partially at the ground and use\n\
 * this to make an intial guess of the feature's depth. This should be enabled\n\
 * for drones that have known-height landing gear.\n\
 *\n\
 * camera_height_off_ground_m: camera distance above ground (meters) for the\n\
 * above bootstrap feature.\n\
 *\n\
 * enable_init_while_moving: optionally allow the algorithm to initialize or\n\
 * reinitialize while moving. Use this if you want to be able to reinitialize\n\
 * during flight. Based on camera motion, not IMU motion. Recommended to leave\n\
 * this off unless absolutely desired.\n\
 *\n\
 * cam_imu_timeshift_s: Misalignment between camera and imu timestamp in\n\
 * seconds.\n\
 *\n\
 * cam_imu_timeshift_s_uncertainty: uncertainty in camera imu timestamp\n\
 * misalignment\n\
 *\n\
 * T_cam_wrt_imu_uncertainty[3] & R_cam_to_imu_uncertainty[3]: uncertainty in\n\
 * camera-imu translation\n\
 *\n\
 * accl_fsr_ms2 & gyro_fsr_rad: Full scale range used to detect clipping. By\n\
 * default this is set to a little under the real 16G and 2000DPS FSR so\n\
 * clipping is detected reliably\n\
 *\n\
 * accl_noise_std_dev & gyro_noise_std_dev: standard deviation of accl and gyro\n\
 * noise\n\
 *\n\
 * cam_noise_std_dev: Standard dev of camera noise per pixel.\n\
 *\n\
 * min_std_pixel_noise: Minimum of standard deviation of feature measurement\n\
 * noise in pixels.\n\
 *\n\
 * fail_high_pixel_noise_points: Scales measurement noise and compares against\n\
 * search area (is search area large enough to reliably compute measurement\n\
 * noise covariance matrix).\n\
 *\n\
 * limited_imu_bw_trigger: To prevent tracking failure during/right after (hard)\n\
 * landing: If sum of 3 consecutive accelerometer samples in any dimension\n\
 * divided by 4.3 exceed this threshold, IMU measurement noise is increased (and\n\
 * resets become more likely); if platform vibrates heavily during flight, this\n\
 * may trigger mid- flight; if poseQuality in mvVISLAMPose drops to\n\
 * MV_TRACKING_STATE_LOW_QUALITY during flight, improve mechanical dampening\n\
 * (and/or increase threshold)\n\
 *\n\
 * gps_imu_time_alignment_s: Misalignment between GPS and IMU time in seconds\n\
 *\n\
 * T_gps_wrt_imu: location of gps with respect to the IMU in meters\n\
 *\n\
 * enable_mapping: rudimentary lightweight mapping of feature points, leave this\n\
 * on.\n\
 */\n"

static char imu_name[MODAL_PIPE_MAX_DIR_LEN];	// name read from config file, used for pipe and cal files
static char cam_name[MODAL_PIPE_MAX_DIR_LEN];	// name read from config file, used for pipe and cal files
static char imu_pipe_dir[MODAL_PIPE_MAX_DIR_LEN];  // full pipe directory constructed from temp name
static char cam_pipe_dir[MODAL_PIPE_MAX_DIR_LEN];  // full pipe directory constructed from temp name
static float odr_hz;
static int use_camera_height_bootstrap;
static float camera_height_off_ground_m;
static int enable_init_while_moving;
static float cam_imu_timeshift_s;
static float cam_imu_timeshift_s_uncertainty;
static float T_cam_wrt_imu_uncertainty[3];
static float R_cam_to_imu_uncertainty[3];
static float accl_fsr_ms2;
static float gyro_fsr_rad;
static float accl_noise_std_dev;
static float gyro_noise_std_dev;
static float cam_noise_std_dev;
static float min_std_pixel_noise;
static float fail_high_pixel_noise_points;
static float limited_imu_bw_trigger;
static float gps_imu_time_alignment_s;
static float T_gps_wrt_imu[3];
static int enable_mapping;

static float default_T_cam_wrt_imu_uncertainty[] = {0.005f, 0.005f, 0.005f};
static float default_R_cam_to_imu_uncertainty[] = {0.04f, 0.04f, 0.04f};
static float default_T_gps_wrt_imu[] = {-0.115f, 0.45f, 0.1f};

/**
 * load the config file and populate the above extern variables
 *
 * @return     0 on success, -1 on failure
 */
static void config_file_print(void)
{
	printf("=================================================================\n");
	printf("imu_name:                         %s\n",    imu_name);
	printf("cam_name:                         %s\n",    cam_name);
	printf("odr_hz:                           %6.3f\n", (double)odr_hz);
	printf("use_camera_height_bootstrap:      %d\n",    use_camera_height_bootstrap);
	printf("camera_height_off_ground_m:       %6.3f\n", (double)camera_height_off_ground_m);
	printf("enable_init_while_moving:         %d\n",    enable_init_while_moving);
	printf("cam_imu_timeshift_s:              %6.3f\n", (double)cam_imu_timeshift_s);
	printf("cam_imu_timeshift_s_uncertainty:  %6.3f\n", (double)cam_imu_timeshift_s_uncertainty);
	printf("T_cam_wrt_imu_uncertainty:        %6.3f %6.3f %6.3f\n", (double)T_cam_wrt_imu_uncertainty[0], (double)T_cam_wrt_imu_uncertainty[1], (double)T_cam_wrt_imu_uncertainty[2]);
	printf("R_cam_to_imu_uncertainty:         %6.3f %6.3f %6.3f\n", (double)R_cam_to_imu_uncertainty[0],  (double)R_cam_to_imu_uncertainty[1],  (double)R_cam_to_imu_uncertainty[2]);
	printf("accl_fsr_ms2:                     %6.3f\n", (double)accl_fsr_ms2);
	printf("gyro_fsr_rad:                     %6.3f\n", (double)gyro_fsr_rad);
	printf("accl_noise_std_dev:               %6.3f\n", (double)accl_noise_std_dev);
	printf("gyro_noise_std_dev:               %6.3f\n", (double)gyro_noise_std_dev);
	printf("cam_noise_std_dev:                %6.3f\n", (double)cam_noise_std_dev);
	printf("min_std_pixel_noise:              %6.3f\n", (double)min_std_pixel_noise);
	printf("fail_high_pixel_noise_points:     %6.3f\n", (double)fail_high_pixel_noise_points);
	printf("limited_imu_bw_trigger:           %6.3f\n", (double)limited_imu_bw_trigger);
	printf("gps_imu_time_alignment_s:         %6.3f\n", (double)gps_imu_time_alignment_s);
	printf("T_gps_wrt_imu:                    %6.3f %6.3f %6.3f\n", (double)T_gps_wrt_imu[0], (double)T_gps_wrt_imu[1], (double)T_gps_wrt_imu[2]);
	printf("enable_mapping:                   %d\n",    enable_mapping);
	printf("=================================================================\n");
	return;
}


/**
 * @brief      prints the current configuration values to the screen
 *
 *             this includes all of the extern variables listed above. If this
 *             is called before config_file_load then it will print the default
 *             values.
 */
static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(VOXL_QVIO_SERVER_CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", VOXL_QVIO_SERVER_CONF_FILE);

	cJSON* parent = json_read_file(VOXL_QVIO_SERVER_CONF_FILE);
	if(parent==NULL) return -1;

	json_fetch_string_with_default(	parent, "imu_name",						imu_name, MODAL_PIPE_MAX_DIR_LEN,	"imu1");
	json_fetch_string_with_default(	parent, "cam_name",						cam_name, MODAL_PIPE_MAX_DIR_LEN,	"tracking");
	json_fetch_float_with_default(	parent, "odr_hz",						&odr_hz,						30);
	json_fetch_bool_with_default(	parent, "use_camera_height_bootstrap",	&use_camera_height_bootstrap,	1);
	json_fetch_float_with_default(	parent, "camera_height_off_ground_m",	&camera_height_off_ground_m,	0.1);
	json_fetch_bool_with_default(	parent, "enable_init_while_moving",		&enable_init_while_moving,		1);
	json_fetch_float_with_default(	parent, "cam_imu_timeshift_s",			&cam_imu_timeshift_s,			0.002);
	json_fetch_float_with_default(	parent, "cam_imu_timeshift_s_uncertainty",		&cam_imu_timeshift_s_uncertainty,0.001);
	json_fetch_fixed_vector_float_with_default(parent, "T_cam_wrt_imu_uncertainty",	T_cam_wrt_imu_uncertainty,	3,	default_T_cam_wrt_imu_uncertainty);
	json_fetch_fixed_vector_float_with_default(parent, "R_cam_to_imu_uncertainty",	R_cam_to_imu_uncertainty,	3,	default_R_cam_to_imu_uncertainty);
	json_fetch_float_with_default(	parent, "accl_fsr_ms2",					&accl_fsr_ms2,					156.0);
	json_fetch_float_with_default(	parent, "gyro_fsr_rad",					&gyro_fsr_rad,					34.0);
	json_fetch_float_with_default(	parent, "accl_noise_std_dev",			&accl_noise_std_dev,			0.316);
	json_fetch_float_with_default(	parent, "gyro_noise_std_dev",			&gyro_noise_std_dev,			0.01);
	json_fetch_float_with_default(	parent, "cam_noise_std_dev",			&cam_noise_std_dev,				100.0);
	json_fetch_float_with_default(	parent, "min_std_pixel_noise",			&min_std_pixel_noise,			0.5);
	json_fetch_float_with_default(	parent, "fail_high_pixel_noise_points",	&fail_high_pixel_noise_points,	1.6651f);
	json_fetch_float_with_default(	parent, "limited_imu_bw_trigger",		&limited_imu_bw_trigger,		35);
	json_fetch_float_with_default(	parent, "gps_imu_time_alignment_s",		&gps_imu_time_alignment_s,		0.0);
	json_fetch_fixed_vector_float_with_default(parent, "T_gps_wrt_imu",		T_gps_wrt_imu,	3,	default_T_gps_wrt_imu);
	json_fetch_bool_with_default(	parent, "enable_mapping",				&enable_mapping,				1);

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", VOXL_QVIO_SERVER_CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// no need for this field anymore
	json_remove_if_present(parent, "warmup_frames");

	// construct proper full pipe paths from the provided imu and cam names
	if(pipe_client_construct_full_path(imu_name, imu_pipe_dir)<0){
		fprintf(stderr, "Invalid imu pipe name: %s\n", imu_name);
		return -1;
	}
	if(pipe_client_construct_full_path(cam_name, cam_pipe_dir)<0){
		fprintf(stderr, "Invalid cam pipe name: %s\n", cam_name);
		return -1;
	}


	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The config file was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(VOXL_QVIO_SERVER_CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	return 0;
}






#endif // end #define CONFIG_FILE_H