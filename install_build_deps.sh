#!/bin/bash

# list all your dependencies here
DEPS="libmodal_pipe libmodal_json mv"

# opkg almost always exits error even when it's fine so ignore that
set +e

# make sure opkg config file exists
OPKG_CONF=/etc/opkg/opkg.conf
if [ ! -f ${OPKG_CONF} ]; then
	echo "ERROR: missing ${OPKG_CONF}"
	echo "are you not running in Yocto?"
	exit 1
fi

# make sure voxl-packages is in the opkg config
if ! grep -q "voxl-packages" ${OPKG_CONF}; then
	echo "opkg not configured for voxl-packages repository yet"
	echo "adding repository now"

	sudo echo "src/gz voxl-packages http://voxl-packages.modalai.com/dev" >> ${OPKG_CONF}
fi

## make sure we have the latest package index
sudo opkg update

# install/update each dependency
for i in ${DEPS}; do
	# this will also update if already installed!
	sudo opkg install $i
done



exit 0