/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <getopt.h>

#include <mvVISLAM.h>
#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include <modal_pipe_client.h>
#include <modal_camera_server_interface.h>
#include <modal_imu_server_interface.h>
#include <modal_common_configs.h>

#include <voxl_qvio_server.h>
#include "config_file.h"

// server channels
#define COMPLETE_CH	0
#define SIMPLE_CH	1

// client channels and config
#define IMU_CH	0
#define CAM_CH	1
#define IMU_PIPE_MIN_PIPE_SIZE (1  * 1024 * 1024) // give ourselves huge buffers
#define CAM_PIPE_MIN_PIPE_SIZE (64 * 1024 * 1024) // give ourselves huge buffers
#define CLIENT_NAME	"qvio-server"
#define PROCESS_NAME "qvio-server" // used for PID file name

static int en_debug = 0;
static int en_debug_pos = 0;
static pthread_t data_thread;
static int64_t last_imu_timestamp_ns = 0;
static int64_t last_cam_timestamp_ns = 0;
static mvVISLAM* mv_vislam_ptr;
static int blank_counter = 0;
static int64_t last_time_alignment_ns = 0;

// mutex to protect the cam frame processing during reset
static pthread_mutex_t cam_mtx = PTHREAD_MUTEX_INITIALIZER;

// set any error codes here for publishing in the data structure in addition
// to errors that come from mvVISLAM
static uint32_t global_error_codes = 0;


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-d, --debug                 enable debug prints\n\
-h, --help                  print this help message\n\
-p, --position              print position and rotation\n\
\n");
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",			no_argument,		0, 'd'},
		{"help",			no_argument,		0, 'h'},
		{"position",		no_argument,		0, 'p'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "dhp", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 'p':
			en_debug_pos = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


static int64_t _apps_time_monotonic_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


static void _nanosleep(uint64_t ns){
	struct timespec req,rem;
	req.tv_sec = ns/1000000000;
	req.tv_nsec = ns%1000000000;
	// loop untill nanosleep sets an error or finishes successfully
	errno=0; // reset errno to avoid false detection
	while(nanosleep(&req, &rem) && errno==EINTR){
		req.tv_sec = rem.tv_sec;
		req.tv_nsec = rem.tv_nsec;
	}
	return;
}


static float _qvio_quality(struct mvVISLAMPose pose)
{
	// vio quality uses covariance and a reset counter
	// the whole covarience matrix should be zero in case of blowup
	if(pose.errCovPose[3][3]==0.0f || pose.errCovPose[4][4]==0.0f || pose.errCovPose[5][5]==0.0f){
		return -1.0f;
	}

	// pick the maximum (worst) of the position diagonal entries
	float max = pose.errCovPose[3][3];
	if(pose.errCovPose[4][4]>max) max = pose.errCovPose[4][4];
	if(pose.errCovPose[5][5]>max) max = pose.errCovPose[5][5];

	// covarience is very small. Scale it by 10^5 to make the output more readable
	float conf = 0.00001f / max;

	// return normal value
	return conf;
}


// convert roll/pitch/yaw in degrees from extrinsics config file to axis-angle
static int _tait_bryan_xyz_intrinsic_to_axis_angle(double tb_deg[3], float aa[3])
{
	if(tb_deg==NULL||aa==NULL){
		fprintf(stderr,"ERROR: in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// convert from degree to rad
	double tb_rad[3];
	tb_rad[0] = tb_deg[0]*M_PI/180.0;
	tb_rad[1] = tb_deg[1]*M_PI/180.0;
	tb_rad[2] = tb_deg[2]*M_PI/180.0;

	double c1 = cos(tb_rad[0]/2.0);
	double s1 = sin(tb_rad[0]/2.0);
	double c2 = cos(tb_rad[1]/2.0);
	double s2 = sin(tb_rad[1]/2.0);
	double c3 = cos(tb_rad[2]/2.0);
	double s3 = sin(tb_rad[2]/2.0);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;
	double s1c2 = s1*c2;
	double c1s2 = c1*s2;

	// XYZ
	double w = c1c2*c3 - s1s2*s3;
	double x = s1c2*c3 + c1s2*s3;
	double y = c1s2*c3 - s1c2*s3;
	double z = c1c2*s3 + s1s2*c3;

	double norm = x*x+y*y+z*z;
	if(norm < 0.0001){
		aa[0]=0.0f;
		aa[1]=0.0f;
		aa[2]=0.0f;
		return 0;
	}

	double angle = 2.0*acos(w);
	double scale = angle/sqrt(norm);
	aa[0] = x*scale;
	aa[1] = y*scale;
	aa[2] = z*scale;
	return 0;
}


// control listens for reset commands
static void _control_pipe_cb(int ch, char* string, int bytes, \
								__attribute__((unused)) void* context)
{
	if(strcmp(string, RESET_VIO_SOFT)==0){
		printf("Client requested soft reset\n");
		pthread_mutex_lock(&cam_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 0);
		pthread_mutex_unlock(&cam_mtx);
		return;
	}
	if(strcmp(string, RESET_VIO_HARD)==0){
		printf("Client requested hard reset\n");
		pthread_mutex_lock(&cam_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 1);
		pthread_mutex_unlock(&cam_mtx);
		return;
	}
	if(strcmp(string, BLANK_VIO_CAM_COMMAND)==0){
		printf("Client requested image blank test\n");
		blank_counter = 30;
		return;
	}

	printf("WARNING: Server received unknown command through the control pipe!\n");
	printf("got %d bytes. Command is: %s on ch %d\n", bytes, string, ch);
	return;
}


// print when a new client connects to us
static void _request_pipe_cb(__attribute__((unused))int ch, char* string, \
								__attribute__((unused))int bytes, int client_id,\
								__attribute__((unused)) void* context)
{
	printf("client \"%s\" with id %d has connected\n", string, client_id);
	return;
}


// print when a client disconnects from us
static void _disconnect_cb(__attribute__((unused))int ch, int client_id, char* name, \
											__attribute__((unused)) void* context)
{
	printf("client \"%s\" with id %d has disconnected\n", name, client_id);
	return;
}


// imu callback registered to the imu server
static void _imu_helper_cb(__attribute__((unused))int ch, char* data, int bytes,\
											__attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int i, n_packets;
	imu_data_t* data_array = modal_imu_validate_pipe_data(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(data_array == NULL) return;
	if(n_packets<=0) return;

	// time this in debug mode
	int64_t time_before, process_time;
	if(en_debug) time_before = _apps_time_monotonic_ns();

	// add all data into VIO
	for(i=0; i<n_packets; i++){
		mvVISLAM_AddAccel(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].accl_ms2[0],
									(double)data_array[i].accl_ms2[1], (double)data_array[i].accl_ms2[2]);
		mvVISLAM_AddGyro(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].gyro_rad[0],
									(double)data_array[i].gyro_rad[1], (double)data_array[i].gyro_rad[2]);
		// record last timestamp so the camera thread is aware
		last_imu_timestamp_ns = data_array[i].timestamp_ns;
	}

	if(en_debug){
		process_time = _apps_time_monotonic_ns() - time_before;
		printf("IMU proc time %6.2fms for %d samples\n", ((double)process_time)/1000000.0, n_packets);
	}

	return;
}


// camera frame callback registered to voxl-camera-server
static void _cam_helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta,\
							char* frame, __attribute__((unused)) void* context)
{
	/*
	// todo check resolution later after we load lens calibration file
	if(meta.width != cam_cal.width_pixels || meta.height != cam_cal.height_pixels){
		fprintf(stderr, "ERROR camera frame resolution doesn't match calibration resolution\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_RES;
		return;
	}
	else global_error_codes &= ~ERROR_CODE_CAM_BAD_RES;
	*/

	if(meta.format != IMAGE_FORMAT_RAW8){
		fprintf(stderr, "ERROR only support RAW_8 images right now\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_FORMAT;
		return;
	}
	else global_error_codes &= ~ERROR_CODE_CAM_BAD_FORMAT;

	// prevent camera frames from going in if IMU is still initializing
	if(last_imu_timestamp_ns<=0){
		if(en_debug){
			printf("skipping camera frame while waiting for IMU\n");
		}
		return;
	}

	// Make the timestamp be the center of the exposure
	int64_t cam_timestamp_ns = meta.timestamp_ns;
	cam_timestamp_ns += meta.exposure_ns / 2;

	// don't let image go in until IMU has caught up
	// if cam-imu alignment is POSITIVE that means the camera timestamp is early
	// and the image was actually taken after the reported timestamp
	while(last_imu_timestamp_ns < (cam_timestamp_ns + last_time_alignment_ns)){
		if(!main_running) return;
		if(en_debug){
			printf("waiting for imu\n");
		}
		usleep(10000);
	}

	// if blank counter is positive then blank out the image
	if(blank_counter>0){
		memset(frame, 128, meta.size_bytes);
		blank_counter--;
	}

	// send to mv library, this may take many seconds during reset!!!!
	// time it for now. TODO: logic to handle dropping frames when necessary
	pthread_mutex_lock(&cam_mtx);
	int64_t time_before = _apps_time_monotonic_ns();
	mvVISLAM_AddImage(mv_vislam_ptr, cam_timestamp_ns , (const uint8_t*)frame);
	int64_t process_time = _apps_time_monotonic_ns() - time_before;
	//printf("cam_timestamp_ns: %lld\n", cam_timestamp_ns);

	// record timestamp of camera frame AFTER it's been injested
	last_cam_timestamp_ns = cam_timestamp_ns;
	pthread_mutex_unlock(&cam_mtx);

	if(process_time > 100000000){
		fprintf(stderr, "WARNING slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
	}

	if(en_debug){
		printf("CAM proc time %6.2fms\n", ((double)process_time)/1000000.0);
	}
	return;
}


// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	pipe_server_close_all_channels();
	pipe_client_close_all();
	mvVISLAM_Deinitialize(mv_vislam_ptr);
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// this is the thread to get data from mvVISLAM and publish to the server pipe
static void* _data_thread_func(__attribute__((unused)) void* context)
{
	int64_t next_time = _apps_time_monotonic_ns() + (int64_t)(1000000000/odr_hz);
	int i,j;
	int64_t time_before, process_time;

	// run until the global main_running flag becomes 0
	while(main_running){

		mvVISLAMPose p;	// pose data from mvvislam
		qvio_data_t d;	// complete "extended" vio MPA packet
		vio_data_t s;	// simplified vio packet
		int nPoints;

		//make sure we start with a clean data struct and apply any global error codes
		memset(&d,0,sizeof(d));
		d.magic_number = QVIO_MAGIC_NUMBER;
		d.error_code = global_error_codes;
		memset(&s,0,sizeof(s));
		s.magic_number = QVIO_MAGIC_NUMBER;
		s.error_code = global_error_codes;

		// if we are still waiting for imu/camera to start then report it
		if(last_imu_timestamp_ns == 0) d.error_code|=ERROR_CODE_IMU_MISSING;
		if(last_cam_timestamp_ns == 0) d.error_code|=ERROR_CODE_CAM_MISSING;
		if(last_imu_timestamp_ns == 0 || last_cam_timestamp_ns == 0){
			d.state = VIO_STATE_INITIALIZING;
			s.state = VIO_STATE_INITIALIZING;
			goto SEND;
		}

		// Grab the pose and feature points
		time_before = _apps_time_monotonic_ns();
		p = mvVISLAM_GetPose(mv_vislam_ptr);
		nPoints = mvVISLAM_HasUpdatedPointCloud(mv_vislam_ptr);
		process_time = _apps_time_monotonic_ns() - time_before;

		// grab state from mv
		if(p.poseQuality == MV_TRACKING_STATE_FAILED) d.state = VIO_STATE_FAILED;
		else if(p.poseQuality == MV_TRACKING_STATE_INITIALIZING) d.state = VIO_STATE_INITIALIZING;
		else d.state = VIO_STATE_OK;

		// error code is a bitmask that we might have added to already so OR
		// it with VIOs error code
		d.error_code |= p.errorCode;

		// populate some other data
		d.quality = _qvio_quality(p);
		d.timestamp_ns = p.time;
		d.imu_cam_time_shift_s = p.timeAlignment;
		last_time_alignment_ns = p.timeAlignment * 1000000000;
		d.n_feature_points = nPoints;

		// deconstruct mv's 6DRT struct
		for(i=0;i<3;i++){
			d.T_imu_wrt_vio[i] = p.bodyPose.matrix[i][3];
			for(j=0;j<3;j++) d.R_imu_to_vio[i][j] = p.bodyPose.matrix[i][j];
		}
		for(i=0;i<3;i++){
			d.T_ga_vio_wrt_cam[i] = p.gravityCameraPose.matrix[i][3];
			for(j=0;j<3;j++) d.R_ga_vio_to_cam[i][j] = p.gravityCameraPose.matrix[i][j];
		}

		// memcpy the rest
		memcpy(d.pose_covariance,	p.errCovPose,		sizeof(float)*6*6);
		memcpy(d.vel_imu_wrt_vio,	p.velocity,			sizeof(float)*3);
		memcpy(d.vel_covariance,	p.errCovVelocity,	sizeof(float)*3*3);
		memcpy(d.imu_angular_vel,	p.angularVelocity,	sizeof(float)*3);
		memcpy(d.gravity_vector,	p.gravity,			sizeof(float)*3);
		memcpy(d.gravity_covariance,p.errCovGravity,	sizeof(float)*3*3);
		memcpy(d.gyro_bias,			p.wBias,			sizeof(float)*3);
		memcpy(d.accl_bias,			p.aBias,			sizeof(float)*3);
		memcpy(d.R_accl_to_gyro,	p.Rbg,				sizeof(float)*3*3);
		memcpy(d.accl_orthogonality,p.aAccInv,			sizeof(float)*3*3);
		memcpy(d.gyro_orthogonality,p.aGyrInv,			sizeof(float)*3*3);
		memcpy(d.T_cam_wrt_imu,		p.tbc,				sizeof(float)*3);
		memcpy(d.R_cam_to_imu,		p.Rbc,				sizeof(float)*3*3);

		// also make a simplified struct for the common interface
		s.error_code				= d.error_code;
		s.quality					= d.quality;
		s.timestamp_ns				= d.timestamp_ns;
		memcpy(s.T_imu_wrt_vio,		d.T_imu_wrt_vio,	sizeof(float)*3);
		memcpy(s.R_imu_to_vio,		d.R_imu_to_vio,		sizeof(float)*3*3);
		memcpy(s.vel_imu_wrt_vio,	d.vel_imu_wrt_vio,	sizeof(float)*3);
		memcpy(s.imu_angular_vel,	d.imu_angular_vel,	sizeof(float)*3);
		memcpy(s.gravity_vector,	d.gravity_vector,	sizeof(float)*3);
		memcpy(s.T_cam_wrt_imu,		d.T_cam_wrt_imu,	sizeof(float)*3);
		memcpy(s.R_cam_to_imu,		d.R_cam_to_imu,		sizeof(float)*3*3);
		s.n_feature_points			= d.n_feature_points;
		s.state						= d.state;

SEND:
		// send to both pipes
		pipe_server_send_to_channel(COMPLETE_CH, (char*)&d, sizeof(qvio_data_t));
		pipe_server_send_to_channel(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));

		// for debug only
		if(en_debug){
			printf("get pose time %6.2fms  current state: ", ((double)process_time)/1000000.0);
			modal_vio_print_state(d.state);
			modal_vio_print_error_code(d.error_code);
			printf("\n");
		}
		if(en_debug_pos){
			printf("%6.3f %6.3f %6.3f ", (double)d.T_imu_wrt_vio[0],(double)d.T_imu_wrt_vio[1],(double)d.T_imu_wrt_vio[2]);
			printf("\n");
		}

		// try to maintain output data rate (odr)
		int64_t current_time = _apps_time_monotonic_ns();
		next_time += (int64_t)(1000000000.0f/odr_hz);
		// uh oh, we fell behind, warn and get back on track
		if(next_time<current_time){
			fprintf(stderr, "WARNING: output data thread fell behind\n");
			next_time =  current_time + (int64_t)(1000000000.0f/odr_hz);
		}
		_nanosleep(next_time-current_time);
	}

	return NULL;
}


// main just reads config files, opens pipes, and waits
int main(int argc, char* argv[])
{
	int i, n;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// load config and initialize mv lib
////////////////////////////////////////////////////////////////////////////////

	// start with the qvio config file as it contains imu/camera names
	printf("loading qvio config file\n");
	if(config_file_read()) _quit(-1);
	config_file_print();

	// starting point for cam alignment comes from config file
	last_time_alignment_ns = cam_imu_timeshift_s * 1000000000;

	// now grab the extrinsic relation between the imu and camera specified in
	// the qvio config file
	printf("loading extrinsics config file\n");
	extrinsic_t ext[MAX_EXTRINSICS_IN_CONFIG];
	extrinsic_t cam_wrt_imu_ext;
	if(read_extrinsic_conf_file(EXTRINSICS_PATH, ext, &n, MAX_EXTRINSICS_IN_CONFIG)) _quit(-1);
	if(find_extrinsic_in_array(imu_name, cam_name, ext, n, &cam_wrt_imu_ext)){
		fprintf(stderr, "failed to find extrinsics from %s to %s in %s\n", imu_name, cam_name, EXTRINSICS_PATH);
		_quit(-1);
	}
	print_extrinsic_conf(&cam_wrt_imu_ext, 1);
	float tbc[3];
	float ombc[3];
	for(i=0;i<3;i++){
		tbc[i] = cam_wrt_imu_ext.T_child_wrt_parent[i];
	}
	_tait_bryan_xyz_intrinsic_to_axis_angle(cam_wrt_imu_ext.RPY_parent_to_child, ombc);


	printf("tbc:  %6.3f %6.3f %6.3f\n", (double)tbc[0], (double)tbc[1], (double)tbc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (axis angle)\n", (double)ombc[0], (double)ombc[1], (double)ombc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (RPY deg)\n", \
							(double)cam_wrt_imu_ext.RPY_parent_to_child[0],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[1],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[2]);


	// todo grab cam cal from opencv calibration file
	mvCameraConfiguration mv_cam_conf;
	mv_cam_conf.pixelWidth			= 640;
	mv_cam_conf.pixelHeight			= 480;
	mv_cam_conf.memoryStride		= 640;
	mv_cam_conf.uvOffset			= 0;
	mv_cam_conf.principalPoint[0]	= 319.625;
	mv_cam_conf.principalPoint[1]	= 243.144;
	mv_cam_conf.focalLength[0]		= 275.078;
	mv_cam_conf.focalLength[1]		= 274.931;
	mv_cam_conf.distortion[0]		= 0.003908;
	mv_cam_conf.distortion[1]		= -0.009574;
	mv_cam_conf.distortion[2]		= 0.010173;
	mv_cam_conf.distortion[3]		= -0.003329;
	mv_cam_conf.distortion[4]		= 0;
	mv_cam_conf.distortion[5]		= 0;
	mv_cam_conf.distortion[6]		= 0;
	mv_cam_conf.distortion[7]		= 0;
	mv_cam_conf.distortionModel		= 10; // fisheye

	// static parameters
	float tba[3] = {0, 0, 0}; // position of gps relative to imu
	char* staticMaskFileName = NULL; // for image mask
	float logDepthBootstrap = log(1.0); // unused depth bootstrap

	// pass it all in
	mv_vislam_ptr = mvVISLAM_Initialize(
		&mv_cam_conf,
		cam_imu_timeshift_s,
		tbc,
		ombc,
		cam_imu_timeshift_s,
		T_cam_wrt_imu_uncertainty,
		R_cam_to_imu_uncertainty,
		cam_imu_timeshift_s_uncertainty,
		accl_fsr_ms2,
		gyro_fsr_rad,
		accl_noise_std_dev,
		gyro_noise_std_dev,
		cam_noise_std_dev,
		min_std_pixel_noise,
		fail_high_pixel_noise_points,
		logDepthBootstrap,
		use_camera_height_bootstrap,
		log(camera_height_off_ground_m),
		!enable_init_while_moving,
		limited_imu_bw_trigger,
		staticMaskFileName,
		gps_imu_time_alignment_s,
		tba,
		enable_mapping);

	if(mv_vislam_ptr == NULL){
		fprintf(stderr, "Error creating mvVISLAM object\n");
		_quit(-1);
	}

	printf("Please ignore the error about Configuration.SF.xml above. ^^^\n");
	printf("It's an optional file, and should be a warning not an error\n");


////////////////////////////////////////////////////////////////////////////////
// start the server pipe and data thread to write to it
////////////////////////////////////////////////////////////////////////////////

	// init complete pipe
	if(pipe_server_init_channel(COMPLETE_CH, QVIO_EXTENDED_PIPE_DIR, \
												SERVER_FLAG_EN_CONTROL_PIPE)){
		_quit(-1);
	}
	pipe_server_set_default_pipe_size(COMPLETE_CH, VIO_RECOMMENDED_PIPE_SIZE);
	pipe_server_set_control_cb(COMPLETE_CH, _control_pipe_cb, NULL);
	pipe_server_set_request_cb(COMPLETE_CH, _request_pipe_cb, NULL);
	pipe_server_set_disconnect_cb(COMPLETE_CH, _disconnect_cb, NULL);

	// init simple pipe
	if(pipe_server_init_channel(SIMPLE_CH, QVIO_PIPE_DIR, \
												SERVER_FLAG_EN_CONTROL_PIPE)){
		_quit(-1);
	}
	pipe_server_set_default_pipe_size(SIMPLE_CH, VIO_RECOMMENDED_PIPE_SIZE);
	pipe_server_set_control_cb(SIMPLE_CH, _control_pipe_cb, NULL);
	pipe_server_set_request_cb(SIMPLE_CH, _request_pipe_cb, NULL);
	pipe_server_set_disconnect_cb(SIMPLE_CH, _disconnect_cb, NULL);

	// indicate to the soon-to-be-started thread that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running=1;

	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	pthread_create(&data_thread, &tattr, _data_thread_func, NULL);

////////////////////////////////////////////////////////////////////////////////
// now subscribe to cam and imu servers, waiting if they are not alive yet
// VIO needs IMU before camera so init in that order.
////////////////////////////////////////////////////////////////////////////////

	int ret;
	printf("Trying to open imu pipe: %s\n", (char*)imu_pipe_dir);
	while(main_running){
		// try to open a pipe to the imu server
		ret = pipe_client_init_channel(IMU_CH, imu_pipe_dir, CLIENT_NAME, \
										EN_PIPE_CLIENT_SIMPLE_HELPER, \
										IMU_RECOMMENDED_READ_BUF_SIZE);
		pipe_client_set_pipe_size(IMU_CH, IMU_PIPE_MIN_PIPE_SIZE);
		// success, set callback and go into the next step
		if(ret==0){
			pipe_client_set_simple_helper_cb(IMU_CH, _imu_helper_cb, NULL);
			break;
		}
		// server not online yet wait.
		if(ret == PIPE_ERROR_SERVER_NOT_AVAILABLE){
			fprintf(stderr, "waiting for imu\n");
			usleep(500000);
			continue;
		}
		fprintf(stderr, "ERROR: critical failure subscribing to imu pipe\n");
		_quit(-1);
	}

	printf("Trying to open cam pipe: %s\n", (char*)cam_pipe_dir);
	while(main_running){
		// try to open a pipe to the imu server
		ret = pipe_client_init_channel(CAM_CH, cam_pipe_dir, CLIENT_NAME, \
												EN_PIPE_CLIENT_CAMERA_HELPER, 0);
		pipe_client_set_pipe_size(CAM_CH, CAM_PIPE_MIN_PIPE_SIZE);
		// success, set callback and go into the next step
		if(ret==0){
			pipe_client_set_camera_helper_cb(CAM_CH, _cam_helper_cb, NULL);
			break;
		}
		// server not online yet wait.
		if(ret == PIPE_ERROR_SERVER_NOT_AVAILABLE){
			fprintf(stderr, "waiting for camera\n");
			usleep(500000);
			continue;
		}
		fprintf(stderr, "ERROR: critical failure subscribing to imu pipe\n");
		_quit(-1);
	}


	// run until start/stop module catches a signal and changes main_running to 0
	while(main_running) usleep(5000000);


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	pthread_join(data_thread, NULL);
	_quit(0);
	return 0;
}
