cmake_minimum_required(VERSION 3.3)

set(CMAKE_C_FLAGS "-g -std=gnu99 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion -Wmissing-prototypes \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_C_FLAGS}")

set(CMAKE_CXX_FLAGS "-g -std=gnu++11 -Wall -Wextra -Wuninitialized \
	-Wunused-variable -Wdouble-promotion \
	-Wmissing-declarations -Werror=undef -Wno-unused-function ${CMAKE_CXX_FLAGS}")

# server and client binaries
add_subdirectory(server)
add_subdirectory(clients)

# also install the headers from common
install(FILES common/voxl_qvio_server.h DESTINATION /usr/include)